class Message < ActiveRecord::Base
  belongs_to :sender, :class_name => 'MessagesManager'
  belongs_to :receiver, :class_name => 'MessagesManager'
end
