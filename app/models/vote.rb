class Vote < ActiveRecord::Base
  belongs_to :voteable, polymorphic: true
  belongs_to :ideas_manager
end
