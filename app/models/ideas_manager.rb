class IdeasManager < ActiveRecord::Base
  has_many :ideas
  has_many :votes
  has_many :comments
end
