class MessagesManager < ActiveRecord::Base
	belongs_to :user
	has_many :notes
	has_many :received_messages, class_name: 'Message', foreign_key: 'receiver_id'
	has_many :sent_messages, class_name: 'Message', foreign_key: 'sender_id'
end
