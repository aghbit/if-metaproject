class Comment < ActiveRecord::Base
  belongs_to :idea
  belongs_to :ideas_manager
  has_many :votes, as: :voteable
end
