# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

minimized = false
$(window).scroll -> 
  if $(this).scrollTop() > 400 and !minimized
    minimized = true
    $(".navbar-fixed-top").addClass "navbar-shrink"
  else if $(this).scrollTop() <= 400
    minimized = false
    $(".navbar-fixed-top").removeClass "navbar-shrink"