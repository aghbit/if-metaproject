class IdeasController < ApplicationController
	def new
		@idea = Idea.new
	end

	def create
	  @idea = Idea.new(idea_params)
	 
	  if @idea.save
	  	redirect_to @idea
	  else
	  	render 'new'
	  end
	end

	def show
	  @idea = Idea.find(params[:id])
	end

	def index
	  @ideas = Idea.all
	end	

	def edit
	  @idea = Idea.find(params[:id])
	end	

	def update
	  @idea = Idea.find(params[:id])
	 
	  if @idea.update(idea_params)
	    redirect_to @idea
	  else
	    render 'edit'
	  end
	end

	private
	  def idea_params
	    params.require(:idea).permit(:title, :description)
	  end
end
