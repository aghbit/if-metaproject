class CreateNotes < ActiveRecord::Migration
  def change
    create_table :notes do |t|
      t.belongs_to :messages_manager
      t.timestamps
    end
  end
end
