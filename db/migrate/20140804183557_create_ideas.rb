class CreateIdeas < ActiveRecord::Migration
  def change
    create_table :ideas do |t|
      t.string :title
      t.text :text
      t.belongs_to :ideas_manager
      t.integer :status
      t.timestamps
    end
  end
end