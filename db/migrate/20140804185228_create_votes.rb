class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.belongs_to :idea
      t.belongs_to :ideas_manager
      t.timestamps
    end
  end
end
