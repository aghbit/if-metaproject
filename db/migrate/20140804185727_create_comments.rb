class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.belongs_to :idea
      t.belongs_to :ideas_manager
      t.text :text
      t.timestamps
    end
  end
end
