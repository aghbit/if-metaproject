class CreateUserGroups < ActiveRecord::Migration
  def change
    create_table :user_groups do |t|
      t.text :description
      t.string :name
      t.timestamps
    end

    create_table :users_user_groups, id: false do |t|
      t.belongs_to :users
      t.belongs_to :user_groups
    end
  end
end
