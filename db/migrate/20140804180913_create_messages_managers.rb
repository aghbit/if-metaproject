class CreateMessagesManagers < ActiveRecord::Migration
  def change
    create_table :messages_managers do |t|
      t.belongs_to :user
      t.timestamps
    end
  end
end
